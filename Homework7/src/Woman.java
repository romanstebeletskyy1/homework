import java.util.Map;
import java.util.Set;

public final class Woman extends Human{


    public void goToSalon() {
        System.out.println("Йду в салон краси");
    }

    @Override
    public void greetPet() {
        Set<Pet> pets = getFamily().getPets();
        for (Pet pet : pets) {
            System.out.println("Мій улюблений , " + pet.getNickname());
        }
    }

    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Map<DayOfWeek,String> schedule) {
        super(name, surname, year, iq, schedule);
    }
}
