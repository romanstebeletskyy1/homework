import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class FamilyTest {
    private Woman mother;
    private Man father;
    private Human child;
    private Family family;

    @BeforeEach
    void setUp() {
        mother = new Woman();
        father = new Man();
        child = new Human();
        family = new Family(father, mother);
    }

    @Test
    void testAddChild() {
        family.addChild(child);
        List children = family.getChildren();
        int expected = 1;
        assertEquals(expected, children.size());
        assertEquals(child, children.get(0));
    }

    @Test
    void testCountFamily() {
        int expected = 2;
        assertEquals(expected, family.countFamily());
    }
    @Test
    void testDeleteChild() {
        List<Human> children = new ArrayList<>(List.of(new Human("Олексій", "efee", 343), new Human("Олена", "32434", 3443)));
        family.setChildren(children);
        Human childRemove = children.get(0);

        family.deleteChild(childRemove);
        List<Human> updatedChildren = family.getChildren();
        assertEquals(1, updatedChildren.size());
        assertEquals(children.get(0), updatedChildren.get(0));
    }

    @Test
    void testDeleteChildNullChild() {
        List<Human> children = new ArrayList<>(List.of(new Human("Олексій", "efee", 343), new Human("Олена", "32434", 3443)));
        Human nonExistingChild = new Human("О", "efegge", 34333);

        family.setChildren(children);
        boolean result = family.deleteChild(nonExistingChild);
        assertFalse(result);
        assertEquals(children, family.getChildren());
    }


    @Test
    void testDeleteChildIndex() {
        List<Human> children = new ArrayList<>(List.of(new Human("Олексій", "efee", 343), new Human("Олена", "32434", 3443)));
        family.setChildren(children);
        int childRemoveIndex = 0;

        family.deleteChild(0);
        List<Human> updatedChildren = family.getChildren();
        assertEquals(1, updatedChildren.size());
        assertEquals(children.get(0), updatedChildren.get(0));
    }

    @Test
    void testDeleteChildInvalidIndex() {
        List<Human> children = List.of(new Human("Олексій", "efee", 343), new Human("Олена", "32434", 3443));
        Human nonExistingChild = new Human("О", "efegge", 34333);

        int invalidIndex = 2;
        family.setChildren(children);
        boolean result = family.deleteChild(2);

        assertFalse(result);
        assertEquals(children, family.getChildren());
    }

    @Test
    void testToString() {
        mother =new Woman ("Олена", "32434", 3443);
        father = new Man("Олексій", "efee", 343);
        family = new Family(father, mother);

        String expected = "Human {name = 'Олексій', surname = 'efee', year = 343, iq = 0, {}}\n" +
                "Human {name = 'Олена', surname = '32434', year = 3443, iq = 0, {}}\n" +
                "[]\n";
        assertEquals(expected, family.toString());
    }

    @Test
    void testBornChildIq(){
        mother = new Woman("Олена", "32434", 3443, 88, Map.of(DayOfWeek.MONDAY,"shopping"));
        father = new Man("Олена", "32434", 3443, 12, Map.of(DayOfWeek.MONDAY,"sleeping"));
        family = new Family(father,mother);
        Human child = family.bornChild();
        assertEquals(50, child.getIq());
    }
    @Test
    void testBornChildFamily(){
        mother = new Woman("Олена", "32434", 3443, 88, Map.of(DayOfWeek.MONDAY,"shopping"));
        father = new Man("Олена", "32434", 3443, 12, Map.of(DayOfWeek.MONDAY,"sleeping"));
        family = new Family(father,mother);
        Human child = family.bornChild();
        assertEquals(family, child.getFamily());
    }

}

