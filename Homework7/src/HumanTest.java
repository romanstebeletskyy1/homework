import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class HumanTest {


    @Test
    void testToString() {
        Human human = new Human("Steve", "jj", 999, 55,
                Map.of(DayOfWeek.MONDAY, "play football", DayOfWeek.FRIDAY, "play computer games"));
        String expected = "Human {name = 'Steve', surname = 'jj', year = 999, iq = 55, {MONDAY=play football, FRIDAY=play computer games}}";
        assertEquals(expected, human.toString());
    }

    @Test
    void testToStringNull() {
        Human human = new Human();
        String expected = "Human {name = 'null', surname = 'null', year = 0, iq = 0, {}}";
        assertEquals(expected, human.toString());
    }

    @Test
    void testEqualsTrue() {
        Human human1 = new Human();
        Human human2 = new Human();

        Human human3 = new Human("Steve", "jj", 999, 55,
                Map.of(DayOfWeek.MONDAY, "play football", DayOfWeek.FRIDAY, "play computer games"));
        Human human4 = new Human("Steve", "jj", 999, 55,
                Map.of(DayOfWeek.MONDAY, "play football", DayOfWeek.FRIDAY, "play computer games"));

        assertTrue(human3.equals(human4));
        assertTrue(human1.equals(human2));
    }

    @Test
    void testEqualsFalse() {
        Human human1 = new Human();
        Human human2 = new Human("Steve", "", 999, 55,
                Map.of(DayOfWeek.MONDAY, "play football", DayOfWeek.FRIDAY, "play computer games"));

        Human human3 = new Human("Steve", "jj", 999, 55,
                Map.of(DayOfWeek.MONDAY, "play football", DayOfWeek.FRIDAY, "play computer games"));

        assertFalse(human3.equals(human2));
        assertFalse(human1.equals(human2));
    }

    @Test
    void testHashCode(){
        Human human1 = new Human();
        Human human2 = new Human();

        Human human3 = new Human("Steve", "jj", 999, 55,
                Map.of(DayOfWeek.MONDAY, "play football", DayOfWeek.FRIDAY, "play computer games"));
        Human human4 = new Human("Steve", "jj", 999, 55,
                Map.of(DayOfWeek.MONDAY, "play football", DayOfWeek.FRIDAY, "play computer games"));

        assertEquals(human1.hashCode(), human2.hashCode());
        assertEquals(human3.hashCode(), human4.hashCode());
    }

    @Test
    void testHashCodeNegative(){
        Human human1 = new Human();
        Human human2 = new Human("Steve", "", 999, 55,
                Map.of(DayOfWeek.MONDAY, "play football", DayOfWeek.FRIDAY, "play computer games"));

        Human human3 = new Human("Steve", "jj", 999, 55,
                Map.of(DayOfWeek.MONDAY, "play football", DayOfWeek.FRIDAY, "play computer games"));

        assertNotEquals(human1.hashCode(), human2.hashCode());
        assertNotEquals(human3.hashCode(), human2.hashCode());
    }
}
