import java.util.*;

public class Family implements HumanCreator{
    Random random = new Random();
    private Woman mother;
    private Man father;
    private List<Human> children= new ArrayList<>();
    private Set<Pet> pets = new HashSet<>();

    static {
        System.out.println("Class Family is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт Family\n");
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPet(Pet pet) {
        this.pets.add(pet);
    }

    public int iqParents(){
        return (getFather().getIq() + getMother().getIq()) / 2;
    }
    @Override
    public Human bornChild() {
        int nameIndex;
        if (random.nextBoolean()) {
            nameIndex = random.nextInt(6);
            Man man = new Man();

            man.setName(manName[nameIndex]);
            man.setFamily(this);
            man.setSurname(getFather().getSurname());

            man.setIq(iqParents());

            return man;
        } else
            nameIndex = random.nextInt(6);
        Woman woman = new Woman();

        woman.setName(womanName[nameIndex]);
        woman.setFamily(this);
        woman.setSurname(getFather().getSurname());

        woman.setIq(iqParents());

        return woman;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (father == null && mother == null) {
            father = new Man();
            mother = new Woman();
            if (this.pets == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pets.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
            father = null;
            mother = null;
        } else if (father == null) {
            father = new Man();
            mother = new Woman();
            if (this.pets == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pets.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
            father = null;
        } else if (mother == null) {
            father = new Man();
            mother = new Woman();
            if (this.pets == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pets.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
            mother = null;
        }else {
            if (this.pets == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pets.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
        }
        return sb.toString();
    }

    @Override
    protected void finalize() {
        System.out.println("Family is deleting");
    }

    public void addChild(Human child) {
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size()) {
            return false;
        }
        children.remove(index);
        return true;
    }

    public boolean deleteChild(Human child) {
        child.setFamily(null);
      return children.remove(child);
    }

    public int countFamily() {
        return children.size() + 2;
    }

    public Family(Man father, Woman mother) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
    }
}
