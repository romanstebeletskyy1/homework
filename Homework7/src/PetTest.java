import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class PetTest {
    @Test
    void testToString(){
        Fish fish = new Fish("eve", 13, 100, Set.of("flying, sleeping"));
       String expected = "FISH {can fly = false, number of legs = 0, has fur = false nickname = 'eve', age = 13, trickLevel = 100, habits = [flying, sleeping]}";
       assertEquals(expected,fish.toString());
    }

    @Test
    void testToStringNull(){
        Dog pet = new Dog();
        String expected = "DOG {can fly = false, number of legs = 4, has fur = true nickname = 'null', age = 0, trickLevel = 0, habits = []}";
        assertEquals(expected,pet.toString());
    }

    @Test
    void testEqualsTrue(){
        Fish fish1 = new Fish("eve", 13, 100, Set.of("flying", "sleeping"));
        Fish bird2 = new Fish("eve", 13, 100, Set.of("flying", "sleeping"));

        Dog dog1 = new Dog();
        Dog dog2 = new Dog();
        assertTrue(dog1.equals(dog2));
        assertTrue(fish1.equals(bird2));
    }

    @Test
    void testEqualsFalse(){
        Dog dog = new Dog();
        RoboCat bird = new RoboCat("eve", 13, 100, Set.of("flying", "sleeping"));

        assertFalse(bird.equals(dog));
    }

    @Test
    void testHashCode(){
        Fish bird1 = new Fish("eve", 13, 100, Set.of("flying", "sleeping"));
        Fish bird2 = new Fish( "eve", 13, 100, Set.of("flying", "sleeping"));

        Dog dog1 = new Dog();
        Dog dog2 = new Dog();

        assertEquals(dog1.hashCode(), dog2.hashCode());
        assertEquals(bird1.hashCode(), bird2.hashCode());
    }

    @Test
    void testHashCodeNegative(){
        Fish bird1 = new Fish("eve", 13, 100, Set.of("flying", "sleeping"));
        Fish bird2 = new Fish("eve");

        Dog dog1 = new Dog("F");
        Dog dog2 = new Dog();

        assertNotEquals(dog1.hashCode(), dog2.hashCode());
        assertNotEquals(bird1.hashCode(), bird2.hashCode());
    }
}
