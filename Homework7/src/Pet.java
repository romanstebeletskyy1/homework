import java.util.*;


public abstract class Pet implements PetFoul {
    Random random = new Random();
    private final Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits = new HashSet<>();
    //   private String[] habits;
    private boolean timeToEat;

    static {
        System.out.println("Class Pet is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт Pet\n");
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public boolean getTimeToEat() {
        int bool = random.nextInt(2);
        if (bool == 0) {
            return true;
        } else {
            return false;
        }
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void eat() {
        System.out.println("Я їм!");
    }

    public abstract void respond();

    @Override
    public void foul() {;
    }

    @Override
    public String toString(){
        if(getSpecies() == null){
            return String.format("{nickname = '%s', age = %d, trickLevel = %d, habits = %s}", getNickname(), getAge(), getTrickLevel(),getHabits());
        }else{
            return String.format(species.name() + " {can fly = " + species.canFly() + ", " +
                    "number of legs = " + species.getNumberOfLegs() + ", " + "has fur = " + species.hasFur() +
                    " nickname = '%s', age = %d, trickLevel = %d, habits = %s}", getNickname(), getAge(), getTrickLevel(), getHabits());
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pet pet = (Pet) o;
        if (pet.hashCode() != o.hashCode()) {
            return false;
        }

        if (age != pet.age || trickLevel != pet.trickLevel) {
            return false;
        }
        if (nickname == null && pet.nickname == null) {
            return true;
        }
        if (nickname == null || pet.nickname == null) {
            return false;
        }
        if (!nickname.equals(pet.nickname)) {
            return false;
        }
        return species.equals(pet.species);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }

    @Override
    protected void finalize() {
        System.out.println("Pet is deleting");
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet() {
    }
}
