import java.util.Arrays;
import java.util.Set;

public class Fish extends Pet{
    private final Species species = Species.FISH;

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond(){
        System.out.println("Буль буль буль...");
    };

    @Override
    public String toString(){
        if(getSpecies() == null){
            return String.format("{nickname = '%s', age = %d, trickLevel = %d, habits = %s}", getNickname(), getAge(), getTrickLevel(),getHabits());
        }else{
            return String.format(species.name() + " {can fly = " + species.canFly() + ", " +
                    "number of legs = " + species.getNumberOfLegs() + ", " + "has fur = " + species.hasFur() +
                    " nickname = '%s', age = %d, trickLevel = %d, habits = %s}", getNickname(), getAge(), getTrickLevel(), getHabits());
        }

    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish() {
    }
}
