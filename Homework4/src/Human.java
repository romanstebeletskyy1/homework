import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    Random random = new Random();
    int n;
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private Pet pet;
    private String[][] schedule = new String[n][n];

    static {
        System.out.println("Class Human is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт Human\n");
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String getName() {
        return name;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return year;
    }

    public void setAge(int age) {
        this.year = age;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }


    @Override
    public String toString() {
        return String.format("Human {name = '%s', surname = '%s', year = %d, iq = %d, "
                        + Arrays.deepToString(this.schedule) + "}", this.name, this.surname, this.year,
                this.iq);
    }

    public void greetPet() {
        System.out.println("Привіт, " + pet.getNickname());
    }

    public void describePet() {
        if (pet.getTrickLevel() <= 50) {
            System.out.println("В мене є " + pet.getSpecies() + ", йому " + pet.getAge() + " років, він майже не хитрий");
        } else {
            System.out.println("В мене є " + pet.getSpecies() + ", йому " + pet.getAge() + " років, він дуже хитрий");
        }
    }

    public boolean feedPet(boolean timeToEat) {
        int extraEating = random.nextInt(101);
        if (timeToEat) {
            System.out.println("Годую вихованця");
            return true;
        } else if (pet.getTrickLevel() >= extraEating) {
            System.out.println("Хм... напевно поголдую я " + pet.getNickname());
            return true;
        } else {
            System.out.println("Думаю, " + pet.getNickname() + " не голодний.");
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        if (human.hashCode() != o.hashCode()) {
            return false;
        }

        if (year != human.year || iq != human.iq) {
            return false;
        }
        if (name == null || human.name == null) {
            return false;
        }
        if (!name.equals(human.name)) {
            return false;
        }
        return surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }

    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;

    }
}
