public class Main {
    public static void main(String[] args) {
        Human humanA1 = new Human("Bob", "Smith", 1999, 99,
                new String[][]{{"Monday", "play football"}, {"Friday", "play computer games"}});
        Human humanA2 = new Human("Anna", "Smith", 2000);
        Human humanA3 = new Human();
        Pet dog = new Pet("dog", "steve", 3, 44, new String[]{"playing with ball", "sleeping"});
        Family smiths = new Family(humanA1, humanA2, dog);

        Human humanB1 = new Human("Sean", "Shevchenko", 1978, 56,
                new String[][]{{"Tuesday", "go to gym"}, {"Friday", "play poker with friends"}});
        Human humanB2 = new Human("Olga", "Shevchenko", 1979);
        Human humanB3 = new Human();
        Pet cat = new Pet("cat", "snow");
        Family shevchenko = new Family(humanB1, humanB2);


        humanA3.setPet(dog);
        System.out.println(humanA1.toString());
        System.out.println(humanA2.toString());
        System.out.println(humanA3.toString());
        humanA3.describePet();
        humanA3.greetPet();
        dog.eat();
        dog.foul();
        dog.respond();
        System.out.println(humanA3.feedPet(dog.getTimeToEat()));
        smiths.addChild(humanA3);
        System.out.println(smiths.countFamily());
        System.out.println(smiths.deleteChild(humanA3));
        System.out.println(smiths.countFamily());
        System.out.println(dog.equals(cat));
        System.out.println(humanA1.equals(humanA3));


        System.out.println();

        humanB3.setPet(cat);
        System.out.println(humanB1.toString());
        System.out.println(humanB2.toString());
        System.out.println(humanB3.toString());
        humanB3.describePet();
        humanB3.greetPet();
        cat.eat();
        cat.foul();
        cat.respond();
        System.out.println(humanB3.feedPet(cat.getTimeToEat()));
        System.out.println(shevchenko.countFamily());

        System.out.println("\n\n\n");

        System.out.println(smiths.toString());

        System.out.println();

        System.out.println(shevchenko.toString());
    }
}

