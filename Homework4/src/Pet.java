import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Pet {
    Random random = new Random();
    int n;
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    String[] habits = new String[n];
    private boolean timeToEat;

    static {
        System.out.println("Class Pet is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт Pet\n");
    }

    public boolean getTimeToEat() {
        int bool = random.nextInt(2);
        if (bool == 0) {
            return true;
        } else {
            return false;
        }
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я їм!");
    }

    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + this.nickname + ". Я скучив!");
    }

    public void foul() {
        System.out.println("Треба добре замести сліди...");
    }

    @Override
    public String toString() {
        return String.format("%s {nickname = '%s', age = %d, trickLevel = %d, habits = "
                + Arrays.toString(this.habits) + "}", this.species, this.nickname, this.age, this.trickLevel);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pet pet = (Pet) o;
        if (pet.hashCode() != o.hashCode()) {
            return false;
        }

        if (age != pet.age || trickLevel != pet.trickLevel) {
            return false;
        }
        if (nickname == null || pet.nickname == null) {
            return false;
        }
        if (!nickname.equals(pet.nickname)) {
            return false;
        }
        return species.equals(pet.species);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.species = species;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String species, String nickname) {
        this.nickname = nickname;
        this.species = species;
    }

    public Pet() {
    }
}
