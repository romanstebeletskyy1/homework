package structure.dao;

import structure.model.Family;
import structure.model.Human;

import java.util.List;

public interface FamilyDao {
     List<Family> getAllFamilies();
     Family getFamilyByIndex(int familyIndex);
     boolean deleteFamily (int familyIndex);
     boolean deleteFamily(Family family);
     void saveFamily(Family family);
     int getFamilySize(Family family);
     void setChild(Family family, Human child);
}
