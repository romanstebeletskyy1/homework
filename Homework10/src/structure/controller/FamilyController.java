package structure.controller;
import structure.model.*;
import structure.service.FamilyService;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;
public class FamilyController {
    private final static Scanner scanner = new Scanner(System.in);
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public  void doAll() {
        Man man1 = new Man("Bob", "Smith", 971872036232L, 99, Map.of(DayOfWeek.MONDAY, "play football"));
        Woman woman1 = new Woman("Anna", "Smith", 2000);
        Man child1 = new Man();
        Dog dog = new Dog("steve", 3, 44, Set.of("playing with ball"));
        Family smiths = new Family("smiths",man1, woman1);
        smiths.setPet(dog);
        familyService.saveFamily(smiths);


        Man man2 = new Man("Sean", "Shevchenko", 18, 56, Map.of(DayOfWeek.MONDAY, "go to gym"));
        Woman woman2 = new Woman("Olga", "Shevchenko", 1979);
        Woman child2 = new Woman();
        DomesticCat cat = new DomesticCat("snow");
        Family shevchenko = new Family("shevchenko",man2, woman2);
        shevchenko.setPet(cat);
        familyService.saveFamily(shevchenko);


        System.out.println(man1.toString());
        System.out.println(woman1.toString());
        System.out.println(child1.toString());
        woman1.describePet();
        woman1.greetPet();
        dog.eat();
        dog.foul();
        dog.respond();
        System.out.println(woman1.feedPet(dog.getTimeToEat()));
        smiths.addChild(child1);
        System.out.println(familyService.getFamilySize(smiths));
        System.out.println(smiths.deleteChild(child1));
        System.out.println(familyService.getFamilySize(smiths));
        System.out.println(dog.equals(cat));
        System.out.println(man1.equals(child1));


        System.out.println();


        System.out.println(cat);
        System.out.println(man2.toString());
        System.out.println(woman2.toString());
        System.out.println(child2.toString());
        man2.describePet();
        man2.greetPet();
        cat.eat();
        cat.foul();
        cat.respond();
        System.out.println(man2.feedPet(cat.getTimeToEat()));
        System.out.println(familyService.getFamilySize(shevchenko));

        System.out.println("\n\n\n");

        System.out.println(smiths.toString());

        System.out.println();

        System.out.println(shevchenko.toString());

       Family john = familyService.bornChild(smiths, "L", "d");
      familyService.adoptChild(john, man2);

        System.out.println(familyService.showFamilyMembers(john));
        System.out.println(familyService.getFamilySize(john));
        familyService.deleteAllChildrenOlderThen(1);
        System.out.println(familyService.getFamilySize(john));
        familyService.addPet(0,new Fish());


        familyService.displayAllFamilies();
        familyService.getFamiliesBiggerThan(1);
        familyService.getFamiliesLessThan(3);
        Family family = familyService.getFamilyById(0);
        System.out.println(family);


        System.out.println(familyService.count());
        familyService.deleteFamilyByIndex(0);
        System.out.println(familyService.count());

        System.out.println(man1);
        System.out.println(man1.describeAge());

Human adoptChild = new Human("igor", "shevchenko", "07/07/1977",55);
        System.out.println(adoptChild);
    }
}
