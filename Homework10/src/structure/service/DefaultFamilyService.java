package structure.service;

import structure.dao.FamilyDao;
import structure.model.*;

import java.util.List;
import java.util.Random;

public class DefaultFamilyService implements FamilyService {
    Random random = new Random();
    private FamilyDao familyDao;

    public DefaultFamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }


    @Override
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    @Override
    public void displayAllFamilies() {
        familyDao.getAllFamilies().stream()
                .forEach(e -> {
                    System.out.println("Family #" + getAllFamilies().indexOf(e));
                            System.out.println("Family Name: " + e.getName());
                            System.out.println("Family Members: " + showFamilyMembers(e) + "\n");
                });
        }


    @Override
    public void getFamiliesBiggerThan(int number) {
        List<Family> filteredFamilies = familyDao.getAllFamilies().stream()
                .filter(e -> familyDao.getFamilySize(e) > number)
                .toList();
        System.out.println(filteredFamilies);
    }

    @Override
    public void getFamiliesLessThan(int number) {
        List<Family> filteredFamilies = familyDao.getAllFamilies().stream()
                .filter(e -> familyDao.getFamilySize(e) < number)
                .toList();
        System.out.println(filteredFamilies);
    }

    @Override
    public int countFamiliesWithMemberNumber(int number) {
        List<Family> filteredFamilies = familyDao.getAllFamilies().stream()
                .filter(e -> familyDao.getFamilySize(e) == number)
                .toList();
        return filteredFamilies.size();
    }

    @Override
    public void createNewFamily(Human father, Human mother) {
        father = new Man();
        mother = new Woman();
        Family family = new Family((Man) father, (Woman) mother);
        familyDao.getAllFamilies().add(family);
    }

    @Override
    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamily(index);
    }

    @Override
    public Family bornChild(Family family, String boyName, String girlName) {

        if (random.nextBoolean()) {

            Man man = new Man();
            man.setName(boyName);
            man.setFamily(family);
            man.setSurname(family.getFather().getSurname());
            man.setIq(family.iqParents());
            familyDao.setChild(family, man);

            familyDao.saveFamily(family);
            return family;
        } else {

            Woman woman = new Woman();
            woman.setName(girlName);
            woman.setFamily(family);
            woman.setSurname(family.getFather().getSurname());
            woman.setIq(family.iqParents());

            familyDao.setChild(family, woman);
            familyDao.saveFamily(family);

            return family;
        }
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        familyDao.setChild(family, child);
        familyDao.saveFamily(family);
        return family;
    }

    @Override
    public void deleteAllChildrenOlderThen(int age) {
        List<Family> families = familyDao.getAllFamilies();
        for (Family family : families) {
            family.getChildren().removeIf(child -> child.getBirthDate() > age);
        }
    }

    @Override
    public int count() {
        return familyDao.getAllFamilies().size();
    }

    @Override
    public Family getFamilyById(int index) {
        return familyDao.getAllFamilies().get(index);
    }

    @Override
    public List<Pet> getPets(int index) {
        return familyDao.getAllFamilies().get(index).getPets().stream()
                .toList();
    }

    @Override
    public void addPet(int index, Pet pet) {
        Family family = familyDao.getAllFamilies().get(index);
        family.getPets().add(pet);
        familyDao.saveFamily(family);
    }

    public int getFamilySize(Family family) {
        return familyDao.getFamilySize(family);
    }

    @Override
    public String showFamilyMembers(Family family) {
        String info = String.format("Father: %s %s; Mother: %s %s;"
                , family.getFather().getName(), family.getFather().getSurname()
                , family.getMother().getName(), family.getMother().getSurname());
        StringBuilder sb = new StringBuilder();
        for (Human child : family.getChildren()) {
            sb.append(String.format("Child: %s %s; "
                    , child.getName(), child.getSurname()));
        }
        return info + sb.toString();
    }

    @Override
    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

}
