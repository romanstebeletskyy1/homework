import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[][] schedule = new String[7][2];
        String input;

        schedule[0][0] = "Sunday";
        schedule[0][1] = "do homework";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to gym";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "play football";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to university";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to the pool";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "clean the house";

        String x = schedule[0][0].toUpperCase().trim();

        while (true) {
            System.out.println("Please, input the day of the week: ");
            input = scanner.nextLine().trim().toUpperCase();
            if (input.equalsIgnoreCase("exit")) {
                break;
            }

            switch (input) {
                case "MONDAY":
                    System.out.println("Your tasks for " + schedule[1][0] + ": " + schedule[1][1] + "\n");
                    break;
                case "TUESDAY":
                    System.out.println("Your tasks for " + schedule[2][0] + ": " + schedule[2][1] + "\n");
                    break;
                case "WEDNESDAY":
                    System.out.println("Your tasks for " + schedule[3][0] + ": " + schedule[3][1] + "\n");
                    break;
                case "THURSDAY":
                    System.out.println("Your tasks for " + schedule[4][0] + ": " + schedule[4][1] + "\n");
                    break;
                case "FRIDAY":
                    System.out.println("Your tasks for " + schedule[5][0] + ": " + schedule[5][1] + "\n");
                    break;
                case "SATURDAY":
                    System.out.println("Your tasks for " + schedule[6][0] + ": " + schedule[6][1] + "\n");
                    break;
                case "SUNDAY":
                    System.out.println("Your tasks for " + schedule[0][0] + ": " + schedule[0][1] + "\n");
                    break;

                default:
                    if (input.toUpperCase().trim().startsWith("CHANGE")) {
                        String day = input.substring(6).trim();
                        switch (day) {
                            case "MONDAY":
                                System.out.println("Please, input new tasks for " + schedule[1][0] + ": ");
                                schedule[1][1] = scanner.nextLine();
                                break;
                            case "TUESDAY":
                                System.out.println("Please, input new tasks for " + schedule[2][0] + ": ");
                                schedule[2][1] = scanner.nextLine();
                                break;
                            case "WEDNESDAY":
                                System.out.println("Please, input new tasks for " + schedule[3][0] + ": ");
                                schedule[3][1] = scanner.nextLine();
                                break;
                            case "THURSDAY":
                                System.out.println("Please, input new tasks for " + schedule[4][0] + ": ");
                                schedule[4][1] = scanner.nextLine();
                                break;
                            case "FRIDAY":
                                System.out.println("Please, input new tasks for " + schedule[5][0] + ": ");
                                schedule[5][1] = scanner.nextLine();
                                break;
                            case "SATURDAY":
                                System.out.println("Please, input new tasks for " + schedule[6][0] + ": ");
                                schedule[6][1] = scanner.nextLine();
                                break;
                            case "SUNDAY":
                                System.out.println("Please, input new tasks for " + schedule[0][0] + ": ");
                                schedule[0][1] = scanner.nextLine();
                                break;
                            default:
                                System.out.println("Sorry, I don't understand you, please try again.\n");
                                break;
                        }

                    } else {
                        System.out.println("Sorry, I don't understand you, please try again.\n");
                        break;
                    }
            }

        }
    }
}