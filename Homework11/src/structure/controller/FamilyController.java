package structure.controller;

import structure.model.*;
import structure.service.FamilyService;
import structure.utils.ConsoleUtil;

import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class FamilyController {
    private static final int MAX_FAMILY_MEMBERS = 2;
    private final static Scanner scanner = new Scanner(System.in);
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public void bornChild(Family family, String boyName, String girlName) {
        if (familyService.getFamilySize(family) > MAX_FAMILY_MEMBERS) {
            throw new FamilyOverflowException("Family size exceeds the limit of " + MAX_FAMILY_MEMBERS + " members.");
        }
        familyService.bornChild(family, boyName, girlName);
    }

    public void adoptChild(Family family, Human child) {

        if (familyService.getFamilySize(family) > MAX_FAMILY_MEMBERS) {
            throw new FamilyOverflowException("Family size exceeds the limit of " + MAX_FAMILY_MEMBERS + " members.");
        }
        familyService.adoptChild(family, child);
    }


    public void familyDefault() {
        Man man1 = new Man("Bob", "Smith", 971872036232L, 99, Map.of(DayOfWeek.MONDAY, "play football"));
        Woman woman1 = new Woman("Anna", "Smith", 2000);
        Dog dog = new Dog("steve", 3, 44, Set.of("playing with ball"));
        Family smiths = new Family("smiths", man1, woman1);
        smiths.setPet(dog);
        familyService.saveFamily(smiths);


        Man man2 = new Man("Sean", "Shevchenko", 18, 56, Map.of(DayOfWeek.MONDAY, "go to gym"));
        Woman woman2 = new Woman("Olga", "Shevchenko", 1979);
        DomesticCat cat = new DomesticCat("snow");
        Family shevchenko = new Family("shevchenko", man2, woman2);
        shevchenko.setPet(cat);
        familyService.saveFamily(shevchenko);

    }

    public void createFamily() {
        System.out.println("Enter the name of the mother: ");
        String nameMother = scanner.nextLine();

        System.out.println("Enter the surname of the mother: ");
        String surnameMother = scanner.nextLine();

        System.out.println("Enter the year of birth of the mother (E.g. 1999): ");
        String yearMother = scanner.nextLine();

        System.out.println("Enter the month of birth of the mother (E.g. 07): ");
        String monthMother = scanner.nextLine();

        System.out.println("Enter the day of birth of the mother (E.g. 02): ");
        String dayMother = scanner.nextLine();

        int iqMother = ConsoleUtil.getInputNumberValue(scanner, "Write Mother IQ: ",
                "Invalid input. Please enter a valid option number or 'exit' to quit.");

        System.out.println("Enter the name of the father: ");
        String nameFather = scanner.nextLine();

        System.out.println("Enter the surname of the father: ");
        String surnameFather = scanner.nextLine();

        System.out.println("Enter the year of birth of the father (E.g. 1999): ");
        String yearFather = scanner.nextLine();

        System.out.println("Enter the month of birth of the father (E.g. 07): ");
        String monthFather = scanner.nextLine();

        System.out.println("Enter the day of birth of the father (E.g. 02): ");
        String dayFather = scanner.nextLine();


        int iqFather = ConsoleUtil.getInputNumberValue(scanner, "Write Father IQ: ",
                "Invalid input. Please enter a valid option number or 'exit' to quit.");

        System.out.println("Enter family name: ");
        String nameFamily = scanner.nextLine();
        Human father = new Human();
        Human mother = new Human();

        boolean validDate = false;
        while (!validDate) {
            try {
                mother = new Human(nameMother, surnameMother, String.format("%s/%s/%s", dayMother, monthMother, yearMother), iqMother);
                validDate = true;
            } catch (DateTimeParseException e) {
                System.out.println("Invalid date format for the mother's birthdate. Please try again.");

                System.out.println("Enter the year of birth of the mother (E.g. 1999): ");
                yearMother = scanner.nextLine();

                System.out.println("Enter the month of birth of the mother (E.g. 07): ");
                monthMother = scanner.nextLine();

                System.out.println("Enter the day of birth of the mother (E.g. 02): ");
                dayMother = scanner.nextLine();
            }
        }


        validDate = false;
        while (!validDate) {
            try {
                father = new Human(nameFather, surnameFather, String.format("%s/%s/%s", dayFather, monthFather, yearFather), iqFather);
                validDate = true;
            } catch (DateTimeParseException e) {
                System.out.println("Invalid date format for the father's birthdate. Please try again.");

                System.out.println("Enter the year of birth of the father (E.g. 1999): ");
                yearFather = scanner.nextLine();

                System.out.println("Enter the month of birth of the father (E.g. 07): ");
                monthFather = scanner.nextLine();

                System.out.println("Enter the day of birth of the father (E.g. 02): ");
                dayFather = scanner.nextLine();
            }
        }


familyService.saveFamily( familyService.createNewFamily(father, mother, nameFamily));
    }

    public void editFamilyByIndex() {
        int index;
        boolean exitLoop = false;
        while (!exitLoop) {

            String editFamilyByIndexMenu = """
                    1. Народити дитину
                    2. Усиновити дитину
                    3. Назад
                    """;
            System.out.println(editFamilyByIndexMenu);
            int optionEditByIndex = ConsoleUtil.getInputNumberValue(scanner, "Your option is: ", "Invalid input. Please enter a valid number.");


            switch (optionEditByIndex) {
                case 1 -> {
                    index = ConsoleUtil.getInputNumberValue(scanner, "Enter family index: "
                            , "Invalid input. Please enter a valid index.");
                    System.out.println("Enter the girl's name: ");
                    String nameGirl = scanner.nextLine();
                    System.out.println("Enter the boy's name: ");
                    String nameBoy = scanner.nextLine();
                    try {
                        bornChild(familyService.getFamilyById(index), nameBoy, nameGirl);
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("There is no family with this index");
                        continue;
                    }

                    exitLoop = true;
                }
                case 2 -> {
                    index = ConsoleUtil.getInputNumberValue(scanner, "Enter family index: "
                            , "Invalid input. Please enter a valid index.");

                    System.out.println("Enter the name of the child: ");
                    String nameChild = scanner.nextLine();

                    System.out.println("Enter the surname of the child: ");
                    String surnameChild = scanner.nextLine();

                    System.out.println("Enter the year of birth of the child (E.g. 1999): ");
                    String yearChild = scanner.nextLine();

                    System.out.println("Enter the month of birth of the child (E.g. 07): ");
                    String monthChild = scanner.nextLine();

                    System.out.println("Enter the day of birth of the child (E.g. 02): ");
                    String dayChild = scanner.nextLine();

                    int iqChild = ConsoleUtil.getInputNumberValue(scanner, "Write child IQ: ",
                            "Invalid input. Please enter a valid option number or 'exit' to quit.");


                    Human child = new Human();
                    boolean validDate = false;
                    while (!validDate) {
                        try {
                            child = new Human(nameChild, surnameChild, String.format("%s/%s/%s", dayChild, monthChild, yearChild), iqChild);
                            validDate = true;
                        } catch (DateTimeParseException e) {
                            System.out.println("Invalid date format for the mother's birthdate. Please try again.");

                            System.out.println("Enter the year of birth of the mother (E.g. 1999): ");
                            yearChild = scanner.nextLine();

                            System.out.println("Enter the month of birth of the mother (E.g. 07): ");
                            monthChild = scanner.nextLine();

                            System.out.println("Enter the day of birth of the mother (E.g. 02): ");
                            dayChild = scanner.nextLine();
                        }
                    }

                    try {
                        adoptChild(familyService.getFamilyById(index), child);
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("There is no family with this index");
                        continue;
                    }

                    exitLoop = true;
                }
                case 3 -> {
                    exitLoop = true;
                }
                default -> System.out.println("This option doesn't exist");
            }
        }
    }


    public void menuFamily() {

        System.out.println("Greetings, to continue, choose the option from the list below: ");

        String menu = """
                1. Заповнити тестовими даними
                2. Відобразити весь список сімей
                3. Відобразити список сімей, де кількість людей більша за задану
                4. Відобразити список сімей, де кількість людей менша за задану
                5. Підрахувати кількість сімей, де кількість членів дорівнює
                6. Створити нову родину
                7. Видалити сім'ю за індексом сім'ї у загальному списку
                8. Редагувати сім'ю за індексом сім'ї у загальному списку
                9. Видалити всіх дітей старше віку
                -EXIT-
                """;
        int option = -1;

        while (true) {
            System.out.println(menu);
            System.out.print("Your option is: ");


            String optionStr = scanner.nextLine();

            if (optionStr.trim().equalsIgnoreCase("exit")) {
                break;
            }
            try {
                option = Integer.parseInt(optionStr);
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a valid option number or 'exit' to quit.");
                continue;
            }
            switch (option) {
                case 1 -> {
                    familyDefault();
                    System.out.println("Done!");
                }
                case 2 -> familyService.displayAllFamilies();
                case 3 -> {
                    option = ConsoleUtil.getInputNumberValue(scanner, "Enter an integer: ", "Invalid input. Please enter a valid option number or 'exit' to quit.");
                    familyService.getFamiliesBiggerThan(option);
                }
                case 4 -> {
                    option = ConsoleUtil.getInputNumberValue(scanner, "Enter an integer: ", "Invalid input. Please enter a valid option number or 'exit' to quit.");
                    familyService.getFamiliesLessThan(option);
                }
                case 5 -> {
                    option = ConsoleUtil.getInputNumberValue(scanner, "Enter an integer: ", "Invalid input. Please enter a valid option number or 'exit' to quit.");
                    System.out.println("Кількість сімей, де кількість членів дорівнює " + option + ": " + familyService.countFamiliesWithMemberNumber(option));
                }
                case 6 -> createFamily();
                case 7 -> {
                    option = ConsoleUtil.getInputNumberValue(scanner, "Enter an index of family: ", "Invalid input. Please enter a valid option number or 'exit' to quit.");
                    familyService.deleteFamilyByIndex(option);
                    System.out.println("Family by index " + option + " was deleted!");
                }
                case 8 -> editFamilyByIndex();
                case 9 -> {
                    option = ConsoleUtil.getInputNumberValue(scanner, "Enter an integer: ", "Invalid input. Please enter a valid option number or 'exit' to quit.");
                    familyService.deleteAllChildrenOlderThen(option);
                    System.out.println("All children older than " + option + " were deleted!");
                }
                default -> System.out.println("This option doesn't exist");
            }
        }


    }

}

