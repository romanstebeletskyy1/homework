package structure.controller;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException() {
        super();
    }

    public FamilyOverflowException(String message) {
        super(message);
    }

    public FamilyOverflowException(String message, Throwable cause) {
        super(message, cause);
    }

    public FamilyOverflowException(Throwable cause) {
        super(cause);
    }
}
