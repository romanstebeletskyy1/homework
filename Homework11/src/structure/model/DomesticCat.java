package structure.model;

import java.util.Set;

public class DomesticCat extends Pet{
    private final Species species = Species.CAT;

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + getNickname() + ". Мяу!");
    }

    @Override
    public void foul() {
        System.out.println("Треба тікати");
    }
    @Override
    public String toString(){
        if(getSpecies() == null){
            return String.format("{nickname = '%s', age = %d, trickLevel = %d, habits = %s}", getNickname(), getAge(), getTrickLevel(),getHabits());
        }else{
            return String.format(species.name() + " {can fly = " + species.canFly() + ", " +
                    "number of legs = " + species.getNumberOfLegs() + ", " + "has fur = " + species.hasFur() +
                    " nickname = '%s', age = %d, trickLevel = %d, habits = %s}", getNickname(), getAge(), getTrickLevel(), getHabits());
        }

    }

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat() {
    }
}
