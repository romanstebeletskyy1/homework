package structure.model;

import java.util.Set;

public class Dog extends Pet {
    private final Species species = Species.DOG;

    public Species getSpecies() {
        return this.species;
    }

    @Override
    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + getNickname() + ". ГАВ ГАВ!");
    }

    @Override
    public void foul() {
        System.out.println("Це не я");
    }

    @Override
    public String toString(){
        if(getSpecies() == null){
            return String.format("{nickname = '%s', age = %d, trickLevel = %d, habits = %s}", getNickname(), getAge(), getTrickLevel(),getHabits());
        }else{
            return String.format(species.name() + " {can fly = " + species.canFly() + ", " +
                    "number of legs = " + species.getNumberOfLegs() + ", " + "has fur = " + species.hasFur() +
                    " nickname = '%s', age = %d, trickLevel = %d, habits = %s}", getNickname(), getAge(), getTrickLevel(), getHabits());
        }

    }


    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog() {
    }
}
