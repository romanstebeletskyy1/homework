package structure.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import structure.dao.FamilyDao;
import structure.model.Family;
import structure.model.Human;
import structure.model.Man;
import structure.model.Woman;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DefaultFamilyServiceTest {

    @Mock
    private FamilyDao familyDao;
    @InjectMocks
    private DefaultFamilyService familyService;

    @BeforeEach
    void setUp() {
        familyDao = mock(FamilyDao.class);
        familyService = new DefaultFamilyService(familyDao);
    }

    @Test
    void testCountFamiliesWithMemberNumber() {
        List<Family> families = new ArrayList<>();
        families.add(new Family(new Man(), new Woman()));
        when(familyDao.getAllFamilies()).thenReturn(families);
        when(familyDao.getFamilySize(any(Family.class))).thenReturn(2);

        int count = familyService.countFamiliesWithMemberNumber(2);
        assertEquals(1, count);
    }

    @Test
    void testCreateNewfamily() {
        List<Family> families = new ArrayList<>();
        families.add(new Family(new Man(), new Woman()));
        when(familyDao.getAllFamilies()).thenReturn(families);
        familyService.createNewFamily(new Human(), new Human());
        assertEquals(2, familyDao.getAllFamilies().size());
    }

    @Test
    void testDeleteFamilyByIndex() {
        int index = 0;

        familyService.deleteFamilyByIndex(index);

        verify(familyDao, times(1)).deleteFamily(index);
    }


    @Test
    void testGetFamilyById() {
        List<Family> families = new ArrayList<>();
        Family family1 = new Family(new Man("John", "Smith", 4), new Woman("Jane", "Smith", 40));
        Family family2 = new Family(new Man("Mike", "Holmes", 60), new Woman("Emily", "Holmes", 5));
        families.add(family1);
        families.add(family2);
        when(familyDao.getAllFamilies()).thenReturn(families);

        Family family = familyService.getFamilyById(0);

        assertEquals(family1, family);

    }

    @Test
    public void testDisplayAllFamilies() {
        List<Family> expectedFamilies = new ArrayList<>();
        expectedFamilies.add(new Family(new Man("John", "Smith", 4), new Woman("Jane", "Smith", 40)));
        expectedFamilies.add(new Family(new Man("Mike", "Holmes", 60), new Woman("Emily", "Holmes", 5)));

        when(familyDao.getAllFamilies()).thenReturn(expectedFamilies);

        familyService.displayAllFamilies();
        String actual = "Family #0\n" +
                "Family Name: null\n" +
                "Family Members: Father: John Smith; Mother: Jane Smith;\n" +
                "\n" +
                "Family #1\n" +
                "Family Name: null\n" +
                "Family Members: Father: Mike Holmes; Mother: Emily Holmes;\n";
        String expected = "Family #0\n" +
                "Family Name: null\n" +
                "Family Members: Father: John Smith; Mother: Jane Smith;\n" +
                "\n" +
                "Family #1\n" +
                "Family Name: null\n" +
                "Family Members: Father: Mike Holmes; Mother: Emily Holmes;\n";

        assertEquals(expected,actual);
    }

    @Test
    public void testGetFamiliesBiggerThan() {
        List<Family> families = new ArrayList<>();
        families.add(new Family("dd",new Man("John", "Smith", 4), new Woman("Jane", "Smith", 40)));
        families.add(new Family("xx",new Man("f", "Smaaith", 3), new Woman("232", "we", 23)));
        families.add(new Family("ss",new Man("ds", "s", 3), new Woman("vvd", "we", 23)));

        when(familyDao.getAllFamilies()).thenReturn(families);
familyService.getFamiliesBiggerThan(1);
        int number = 2;
        String actual = "[dd, xx, ss]";
                String expected = "[dd, xx, ss]";


        assertEquals(expected, actual);
    }

    @Test
    public void testGetFamiliesLessThan() {
        List<Family> families = new ArrayList<>();
        families.add(new Family("dd",new Man("John", "Smith", 4), new Woman("Jane", "Smith", 40)));
        families.add(new Family("xx",new Man("f", "Smaaith", 3), new Woman("232", "we", 23)));
        families.add(new Family("ss",new Man("ds", "s", 3), new Woman("vvd", "we", 23)));

        when(familyDao.getAllFamilies()).thenReturn(families);
        familyService.getFamiliesLessThan(1);
        int number = 2;
        String actual = "[]";
        String expected = "[]";

        assertEquals(expected, actual);
    }



    @Test
    public void testAdoptChild() {

        Family family = mock(Family.class);


        Human child = mock(Human.class);


        familyService.adoptChild(family, child);

        verify(familyDao).setChild(eq(family), eq(child));
        verify(familyDao).saveFamily(family);
    }
}

