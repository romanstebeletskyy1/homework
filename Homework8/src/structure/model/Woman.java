package structure.model;

import java.util.Map;
import java.util.Set;

public final class Woman extends Human{


    public void goToSalon() {
        System.out.println("Йду в салон краси");
    }

    @Override
    public void greetPet() {
        Set<Pet> pets = getFamily().getPets();
        for (Pet pet : pets) {
            System.out.println("Мій улюблений , " + pet.getNickname());
        }
    }

    public Woman() {
    }

    public Woman(String name, String surname, int age) {
        super(name, surname, age);
    }

    public Woman(String name, String surname, int age, int iq, Map<DayOfWeek,String> schedule) {
        super(name, surname, age, iq, schedule);
    }
}
