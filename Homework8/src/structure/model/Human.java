package structure.model;

import java.util.*;

public class Human {
    Random random = new Random();
    private Map<DayOfWeek, String> schedule = new HashMap<>();
    private DayOfWeek dayOfWeek;
    private String name;
    private String surname;
    private int age;
    private int iq;
    private Family family;

    static {
        System.out.println("Class model.Human is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт model.Human\n");
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

        public Set<Pet> getPets() {
        return getFamily().getPets();
    }
    public String getName() {
        return name;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    @Override
    public String toString() {
        return String.format("model.Human {name = '%s', surname = '%s', year = %d, iq = %d, %s}", this.name, this.surname, this.age,
                this.iq, this.schedule);
    }

    public void greetPet() {
        if (family != null) {
            Set<Pet> pets = family.getPets();
            for (Pet pet : pets) {
                System.out.println("Привіт, " + pet.getNickname());
            }
        }
    }

    public void describePet() {
        if (family != null) {
            Set<Pet> pets = family.getPets();
            for (Pet pet : pets) {
                if (pet.getTrickLevel() <= 50) {
                    System.out.println("В мене є " + pet.getSpecies() + ", йому " + pet.getAge() + " років, він майже не хитрий");
                } else {
                    System.out.println("В мене є " + pet.getSpecies() + ", йому " + pet.getAge() + " років, він дуже хитрий");
                }
            }
        }
    }

    public boolean feedPet(boolean timeToEat) {
        if (family != null) {
            Set<Pet> pets = family.getPets();
            for (Pet pet : pets) {
                int extraEating = random.nextInt(101);
                if (timeToEat) {
                    System.out.println("Годую вихованця");
                    return true;
                } else if (pet.getTrickLevel() >= extraEating) {
                    System.out.println("Хм... напевно поголдую я " + pet.getNickname());
                    return true;
                } else {
                    System.out.println("Думаю, " + pet.getNickname() + " не голодний.");
                    return false;
                }
            }
        }
        return false;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        if (human.hashCode() != o.hashCode()) {
            return false;
        }

        if (age != human.age || iq != human.iq) {
            return false;
        }
        if (name == null && human.name == null) {
            return true;
        }
        if (name == null || human.name == null) {
            return false;
        }
        if (!name.equals(human.name)) {
            return false;
        }
        return surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, age, iq);
    }

    @Override
    protected void finalize() {
        System.out.println("model.Human is deleting");
    }

    public Human() {
    }

    public Human(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public Human(String name, String surname, int age, int iq, Map<DayOfWeek,String> schedule){
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.iq = iq;
        this.schedule = schedule;

    }
}
