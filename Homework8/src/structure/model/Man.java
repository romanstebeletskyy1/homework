package structure.model;

import java.util.Map;
import java.util.Set;

public final class Man extends Human{
    public void drinkBeer(){
        System.out.println("Йду пити пиво з друзями");
    }

    @Override
    public void greetPet() {
        Set<Pet> pets = getFamily().getPets();
        for (Pet pet : pets) {
            System.out.println("Здоров, " + pet.getNickname());
        }
    }


    public Man() {
    }

    public Man(String name, String surname, int age) {
        super(name, surname, age);
    }

    public Man(String name, String surname, int age, int iq, Map<DayOfWeek,String> schedule) {
        super(name, surname, age, iq, schedule);
    }
}
