package structure.model;

import java.util.*;

public class Family implements HumanCreator {
    Random random = new Random();
    private Woman mother;
    private Man father;
    private List<Human> children= new ArrayList<>();
    private Set<Pet> pets = new HashSet<>();
    private String name;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    static {
        System.out.println("Class model.Family is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт model.Family\n");
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPet(Pet pet) {
        this.pets.add(pet);
    }

    public int iqParents(){
        return (getFather().getIq() + getMother().getIq()) / 2;
    }
    @Override
    public Human bornChild() {
        int nameIndex;
        if (random.nextBoolean()) {
            nameIndex = random.nextInt(6);
            Man man = new Man();

            man.setName(HumanCreator.manName[nameIndex]);
            man.setFamily(this);
            man.setSurname(getFather().getSurname());

            man.setIq(iqParents());

            return man;
        } else
            nameIndex = random.nextInt(6);
        Woman woman = new Woman();

        woman.setName(HumanCreator.womanName[nameIndex]);
        woman.setFamily(this);
        woman.setSurname(getFather().getSurname());

        woman.setIq(iqParents());

        return woman;
    }


    public String toString() {
        if(getName() == null){
            return "";
        }
        return getName();
    }

    @Override
    protected void finalize() {
        System.out.println("model.Family is deleting");
    }

    public void addChild(Human child) {
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size()) {
            return false;
        }
        children.remove(index);
        return true;
    }

    public boolean deleteChild(Human child) {
        child.setFamily(null);
      return children.remove(child);
    }

    public Family(String name, Man father, Woman mother) {
        this.name = name;
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
    }
    public Family( Man father, Woman mother) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
    }
}
