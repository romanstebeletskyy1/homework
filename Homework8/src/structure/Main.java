package structure;

import structure.controller.FamilyController;
import structure.dao.CollectionFamilyDao;
import structure.dao.FamilyDao;
import structure.service.DefaultFamilyService;
import structure.service.FamilyService;

public class Main {
    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new DefaultFamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);
        familyController.doAll();
    }
}
