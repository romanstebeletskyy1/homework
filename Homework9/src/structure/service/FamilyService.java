package structure.service;

import structure.model.Family;
import structure.model.Human;
import structure.model.Pet;

import java.util.List;

public interface FamilyService {
    List<Family> getAllFamilies();

    void displayAllFamilies();

    void getFamiliesBiggerThan(int number);

    void getFamiliesLessThan(int number);

    int countFamiliesWithMemberNumber(int number);
    void createNewFamily(Human father, Human mother);
    void deleteFamilyByIndex(int index);
    Family bornChild(Family family, String boyName, String girlName);
    Family adoptChild(Family family, Human child);
    void deleteAllChildrenOlderThen(int age);
    int count();
    Family getFamilyById(int index);
    List<Pet> getPets(int index);
    void addPet(int index, Pet pet);
    int getFamilySize(Family family);
    String showFamilyMembers(Family family);
    void saveFamily(Family family);
}
