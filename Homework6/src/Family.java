import java.util.Arrays;
import java.util.Random;

public class Family implements HumanCreator{
    Random random = new Random();
    private Woman mother;
    private Man father;
    private Human[] children = new Human[0];
    private Pet pet;

    static {
        System.out.println("Class Family is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт Family\n");
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }


    public int iqParents(){
        return (getFather().getIq() + getMother().getIq()) / 2;
    }
    @Override
    public Human bornChild() {
        int nameIndex;
        if (random.nextBoolean()) {
            nameIndex = random.nextInt(6);
            Man man = new Man();

            man.setName(manName[nameIndex]);
            man.setFamily(this);
            man.setSurname(getFather().getSurname());

            man.setIq(iqParents());

            return man;
        } else
            nameIndex = random.nextInt(6);
        Woman woman = new Woman();

        woman.setName(womanName[nameIndex]);
        woman.setFamily(this);
        woman.setSurname(getFather().getSurname());

        woman.setIq(iqParents());

        return woman;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (father == null && mother == null) {
            father = new Man();
            mother = new Woman();
            if (this.pet == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pet.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
            father = null;
            mother = null;
        } else if (father == null) {
            father = new Man();
            mother = new Woman();
            if (this.pet == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pet.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
            father = null;
        } else if (mother == null) {
            father = new Man();
            mother = new Woman();
            if (this.pet == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pet.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
            mother = null;
        }else {
            if (this.pet == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pet.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
        }
        return sb.toString();
    }

    @Override
    protected void finalize() {
        System.out.println("Family is deleting");
    }

    public void addChild(Human child) {
        child.setFamily(this);
        Human[] updateChildren = Arrays.copyOf(children, children.length + 1);
        updateChildren[children.length] = child;
        children = updateChildren;
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.length) {
            return false;
        }

        Human[] updatedChildren = new Human[children.length - 1];
        int current = 0;
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                updatedChildren[current] = children[i];
                current++;
            }
        }
        children = updatedChildren;
        return true;
    }

    public boolean deleteChild(Human child) {
        child.setFamily(null);
        Human[] updateChildren = new Human[children.length - 1];
        int current = 0;
        int index = -1;
        for (int i = 0; i < children.length; i++) {
            if (child.equals(children[i])) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return false;
        }
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                updateChildren[current] = children[i];
                current++;
            }
        }
        children = updateChildren;
        return true;
    }

    public int countFamily() {
        if (pet == null) {
            if ((father == null && mother != null) || (father != null && mother == null)) {
                return children.length + 1;
            } else if (father == null) {
                return children.length;
            }
            return children.length + 2;
        }
        return children.length + 2;
    }

    public Family(Man father, Woman mother) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
    }
}
