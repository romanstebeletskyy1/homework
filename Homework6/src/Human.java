import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    Random random = new Random();
    private DayOfWeek dayOfWeek;
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] schedule;

    static {
        System.out.println("Class Human is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт Human\n");
    }

    public Pet getPet() {
        return family.getPet();
    }
    public String getName() {
        return name;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return year;
    }

    public void setAge(int age) {
        this.year = age;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }


    @Override
    public String toString() {
        return String.format("Human {name = '%s', surname = '%s', year = %d, iq = %d, "
                        + Arrays.deepToString(this.schedule) + "}", this.name, this.surname, this.year,
                this.iq);
    }

    public void greetPet() {
        System.out.println("Привіт, " + this.family.getPet().getNickname());
    }

    public void describePet() {
        if (this.family.getPet().getTrickLevel() <= 50) {
            System.out.println("В мене є " + this.family.getPet().getSpecies() + ", йому " + this.family.getPet().getAge() + " років, він майже не хитрий");
        } else {
            System.out.println("В мене є " + this.family.getPet().getSpecies() + ", йому " + this.family.getPet().getAge() + " років, він дуже хитрий");
        }
    }

    public boolean feedPet(boolean timeToEat) {
        int extraEating = random.nextInt(101);
        if (timeToEat) {
            System.out.println("Годую вихованця");
            return true;
        } else if (family.getPet().getTrickLevel() >= extraEating) {
            System.out.println("Хм... напевно поголдую я " + family.getPet().getNickname());
            return true;
        } else {
            System.out.println("Думаю, " + family.getPet().getNickname() + " не голодний.");
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        if (human.hashCode() != o.hashCode()) {
            return false;
        }

        if (year != human.year || iq != human.iq) {
            return false;
        }
        if (name == null && human.name == null) {
            return true;
        }
        if (name == null || human.name == null) {
            return false;
        }
        if (!name.equals(human.name)) {
            return false;
        }
        return surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }

    @Override
    protected void finalize() {
        System.out.println("Human is deleting");
    }

    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;

    }
}
