import java.util.Arrays;

public class DomesticCat extends Pet{
    private final Species species = Species.CAT;

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + getNickname() + ". Мяу!");
    }

    @Override
    public void foul() {
        System.out.println("Треба тікати");
    }
    @Override
    public String toString(){
        return String.format(species.name() + " {can fly = " + species.canFly() + ", " +
                "number of legs = " + species.getNumberOfLegs() + ", " + "has fur = " + species.hasFur() +
                " nickname = '%s', age = %d, trickLevel = %d, habits = "
                + Arrays.toString(getHabits()) + "}", getNickname(), getAge(), getTrickLevel());

    }

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat() {
    }
}
