import java.util.Arrays;

public class Dog extends Pet {
    private final Species species = Species.DOG;

    public Species getSpecies() {
        return this.species;
    }

    @Override
    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + getNickname() + ". ГАВ ГАВ!");
    }

    @Override
    public void foul() {
        System.out.println("Це не я");
    }

    @Override
    public String toString() {
        return String.format(this.species.name() + " {can fly = " + this.species.canFly() + ", " +
                "number of legs = " + this.species.getNumberOfLegs() + ", " + "has fur = " + this.species.hasFur() +
                " nickname = '%s', age = %d, trickLevel = %d, habits = "
                + Arrays.toString(getHabits()) + "}", getNickname(), getAge(), getTrickLevel());

    }


    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog() {
    }
}
