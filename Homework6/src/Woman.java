import java.util.Random;

public final class Woman extends Human{


    public void goToSalon() {
        System.out.println("Йду в салон краси");
    }

    @Override
    public void greetPet() {
        System.out.println("Мій улюблений , " + getFamily().getPet().getNickname());
    }


    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }
}
