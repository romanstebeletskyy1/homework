public class MainH6 {
    public static void main(String[] args) {
        Man man1 = new Man("Bob", "Smith", 1999, 99,
                new String[][]{{"Monday", "play football"}, {"Friday", "play computer games"}});
        Woman woman1 = new Woman("Anna", "Smith", 2000);
        Man child1 = new Man();
        Dog dog = new Dog("steve", 3, 44, new String[]{"playing with ball", "sleeping"});
        Family smiths = new Family(man1, woman1);
        smiths.setPet(dog);

        Man man2 = new Man("Sean", "Shevchenko", 1978, 56,
                new String[][]{{DayOfWeek.MONDAY.name(), "go to gym"}, {DayOfWeek.FRIDAY.name(), "play poker with friends"}});
        Woman woman2 = new Woman("Olga", "Shevchenko", 1979);
        Woman child2 = new Woman();
        DomesticCat cat = new DomesticCat("snow");
        Family shevchenko = new Family(man2, woman2);
        shevchenko.setPet(cat);



        System.out.println(man1.toString());
        System.out.println(woman1.toString());
        System.out.println(child1.toString());
        woman1.describePet();
        woman1.greetPet();
        dog.eat();
        dog.foul();
        dog.respond();
        System.out.println(woman1.feedPet(dog.getTimeToEat()));
        smiths.addChild(child1);
        System.out.println(smiths.countFamily());
        System.out.println(smiths.deleteChild(child1));
        System.out.println(smiths.countFamily());
        System.out.println(dog.equals(cat));
        System.out.println(man1.equals(child1));


        System.out.println();


        System.out.println(cat);
        System.out.println(man2.toString());
        System.out.println(woman2.toString());
        System.out.println(child2.toString());
        man2.describePet();
        man2.greetPet();
        cat.eat();
        cat.foul();
        cat.respond();
        System.out.println(man2.feedPet(cat.getTimeToEat()));
        System.out.println(shevchenko.countFamily());

        System.out.println("\n\n\n");

        System.out.println(smiths.toString());

        System.out.println();

        System.out.println(shevchenko.toString());

       Man manX = (Man) smiths.bornChild();

        System.out.println(manX);
        manX.drinkBeer();
        manX.greetPet();

    }
}

