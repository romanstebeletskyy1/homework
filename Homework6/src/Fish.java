import java.util.Arrays;

public class Fish extends Pet{
    private final Species species = Species.FISH;

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond(){
        System.out.println("Буль буль буль...");
    };

    @Override
    public String toString(){
        return String.format(species.name() + " {can fly = " + species.canFly() + ", " +
                "number of legs = " + species.getNumberOfLegs() + ", " + "has fur = " + species.hasFur() +
                " nickname = '%s', age = %d, trickLevel = %d, habits = "
                + Arrays.toString(getHabits()) + "}", getNickname(), getAge(), getTrickLevel());

    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish() {
    }
}
