public final class Man extends Human{
    public void drinkBeer(){
        System.out.println("Йду пити пиво з друзями");
    }

    @Override
    public void greetPet(){
        System.out.println("Здоров, " + getFamily().getPet().getNickname());
    }


    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }
}
