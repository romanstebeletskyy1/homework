import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class PetTest {
    @Test
    void testToString(){
        Pet bird = new Pet(Species.BIRD, "eve", 13, 100, new String[]{"flying", "sleeping"});
       String expected = "BIRD {can fly = true, number of legs = 2, has fur = true nickname = 'eve', age = 13, trickLevel = 100, habits = [flying, sleeping]}";
       assertEquals(expected,bird.toString());
    }

    @Test
    void testToStringNull(){
        Pet pet = new Pet();
        String expected = "{nickname = 'null', age = 0, trickLevel = 0, habits = null}";
        assertEquals(expected,pet.toString());
    }

    @Test
    void testEqualsTrue(){
        Pet bird1 = new Pet(Species.BIRD, "eve", 13, 100, new String[]{"flying", "sleeping"});
        Pet bird2 = new Pet(Species.BIRD, "eve", 13, 100, new String[]{"flying", "sleeping"});

        Pet dog1 = new Pet();
        Pet dog2 = new Pet();
        assertTrue(dog1.equals(dog2));
        assertTrue(bird1.equals(bird2));
    }

    @Test
    void testEqualsFalse(){
        Pet dog = new Pet();
        Pet bird = new Pet(Species.BIRD, "eve", 13, 100, new String[]{"flying", "sleeping"});

        assertFalse(bird.equals(dog));
    }

    @Test
    void testHashCode(){
        Pet bird1 = new Pet(Species.BIRD, "eve", 13, 100, new String[]{"flying", "sleeping"});
        Pet bird2 = new Pet(Species.BIRD, "eve", 13, 100, new String[]{"flying", "sleeping"});

        Pet dog1 = new Pet();
        Pet dog2 = new Pet();

        assertEquals(dog1.hashCode(), dog2.hashCode());
        assertEquals(bird1.hashCode(), bird2.hashCode());
    }

    @Test
    void testHashCodeNegative(){
        Pet bird1 = new Pet(Species.BIRD, "eve", 13, 100, new String[]{"flying", "sleeping"});
        Pet bird2 = new Pet(Species.BIRD, "eve");

        Pet dog1 = new Pet(Species.DOG, "F");
        Pet dog2 = new Pet();

        assertNotEquals(dog1.hashCode(), dog2.hashCode());
        assertNotEquals(bird1.hashCode(), bird2.hashCode());
    }
}
