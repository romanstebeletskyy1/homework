import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

enum Species {
    DOG(false, 4, true),
    CAT(false, 4, true),
    BIRD(true, 2, true),
    FISH(false, 0, false);
    private final boolean canFly;
    private final int numberOfLegs;
    private final boolean hasFur;

    public boolean canFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean hasFur() {
        return hasFur;
    }

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
}

public class Pet {
    Random random = new Random();
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    String[] habits;
    private boolean timeToEat;

    static {
        System.out.println("Class Pet is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт Pet\n");
    }

    public boolean getTimeToEat() {
        int bool = random.nextInt(2);
        if (bool == 0) {
            return true;
        } else {
            return false;
        }
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я їм!");
    }

    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + this.nickname + ". Я скучив!");
    }

    public void foul() {
        System.out.println("Треба добре замести сліди...");
    }

    @Override
    public String toString(){
        if(species == null){
            return String.format("{nickname = '%s', age = %d, trickLevel = %d, habits = "
                    + Arrays.toString(this.habits) + "}", this.nickname, this.age, this.trickLevel);
        }else{
            return String.format(species.name() + " {can fly = "+species.canFly()+", " +
                    "number of legs = "+species.getNumberOfLegs()+", " + "has fur = "+species.hasFur()+
                    " nickname = '%s', age = %d, trickLevel = %d, habits = "
                    + Arrays.toString(this.habits) + "}", this.nickname, this.age, this.trickLevel);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pet pet = (Pet) o;
        if (pet.hashCode() != o.hashCode()) {
            return false;
        }

        if (age != pet.age || trickLevel != pet.trickLevel) {
            return false;
        }
        if (nickname == null && pet.nickname == null) {
            return true;
        }
        if (nickname == null || pet.nickname == null) {
            return false;
        }
        if (!nickname.equals(pet.nickname)) {
            return false;
        }
        return species.equals(pet.species);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }

    @Override
    protected void finalize() {
        System.out.println("Pet is deleting");
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.species = species;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(Species species, String nickname) {
        this.nickname = nickname;
        this.species = species;
    }

    public Pet() {
    }
}
