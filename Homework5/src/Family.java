import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;

    static {
        System.out.println("Class Family is loading...");
    }

    {
        System.out.println("Створюється новий об'єкт Family\n");
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (father == null && mother == null) {
            father = new Human();
            mother = new Human();
            if (this.pet == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pet.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
            father = null;
            mother = null;
        } else if (father == null) {
            father = new Human();
            mother = new Human();
            if (this.pet == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pet.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
            father = null;
        } else if (mother == null) {
            father = new Human();
            mother = new Human();
            if (this.pet == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pet.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
            mother = null;
        }else {
            if (this.pet == null) {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            } else {
                sb.append(this.father.toString()).append("\n")
                        .append(this.mother.toString()).append("\n")
                        .append(this.pet.toString()).append("\n");
                for (Human human : children) {
                    sb.append(human.toString()).append("\n");
                }
            }
        }
        return sb.toString();
    }

    @Override
    protected void finalize() {
        System.out.println("Family is deleting");
    }

    public void addChild(Human child) {
        child.setFamily(this);
        Human[] updateChildren = Arrays.copyOf(children, children.length + 1);
        updateChildren[children.length] = child;
        children = updateChildren;
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.length) {
            return false;
        }

        Human[] updatedChildren = new Human[children.length - 1];
        int current = 0;
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                updatedChildren[current] = children[i];
                current++;
            }
        }
        children = updatedChildren;
        return true;
    }

    public boolean deleteChild(Human child) {
        child.setFamily(null);
        Human[] updateChildren = new Human[children.length - 1];
        int current = 0;
        int index = -1;
        for (int i = 0; i < children.length; i++) {
            if (child.equals(children[i])) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return false;
        }
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                updateChildren[current] = children[i];
                current++;
            }
        }
        children = updateChildren;
        return true;
    }

    public int countFamily() {
        if (pet == null) {
            if ((father == null && mother != null) || (father != null && mother == null)) {
                return children.length + 1;
            } else if (father == null) {
                return children.length;
            }
            return children.length + 2;
        }
        return children.length + 2;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.father = father;
        this.pet = pet;
        this.children = new Human[0];
    }
}
