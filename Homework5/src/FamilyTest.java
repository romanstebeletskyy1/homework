import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {
    private Human mother;
    private Human father;
    private Human child;
    private Family family;

    @BeforeEach
    void setUp() {
        mother = new Human();
        father = new Human();
        child = new Human();
        family = new Family(mother, father);
    }

    @Test
    void testAddChild() {
        family.addChild(child);
        Human[] children = family.getChildren();
        int expected = 1;
        assertEquals(expected, children.length);
        assertEquals(child, children[0]);
    }

    @Test
    void testAddChildEmptyFamily() {
        Family family = new Family(null, null);
        child = new Human("Hh", "dddd", 999);
        family.addChild(child);
        Human[] children = family.getChildren();
        assertEquals(1, children.length);
        assertEquals(child, children[0]);
    }

    @Test
    void testCountFamily() {
        int expected = 2;
        assertEquals(expected, family.countFamily());
    }

    @Test
    void testCountFamilyEmptyFamily() {
        Family family = new Family(null, null);
        int expected = 0;
        assertEquals(expected, family.countFamily());
    }

    @Test
    void testCountFamilyEmptyFather() {
        Family family = new Family(mother, null);
        int expected = 1;
        assertEquals(expected, family.countFamily());
    }

    @Test
    void testCountFamilyEmptyMother() {
        Family family = new Family(null, father);
        int expected = 1;
        assertEquals(expected, family.countFamily());
    }

    @Test
    void testDeleteChild() {
        Human[] children = {new Human("Олексій", "efee", 343), new Human("Олена", "32434", 3443)};
        family.setChildren(children);
        Human childRemove = children[0];

        family.deleteChild(childRemove);
        Human[] updatedChildren = family.getChildren();
        assertEquals(1, updatedChildren.length);
        assertEquals(children[1], updatedChildren[0]);
    }

    @Test
    void testDeleteChildNullChild() {
        Human[] children = {new Human("Олексій", "efee", 343), new Human("Олена", "32434", 3443)};
        Human nonExistingChild = new Human("О", "efegge", 34333);

        family.setChildren(children);
        boolean result = family.deleteChild(nonExistingChild);
        assertFalse(result);
        assertArrayEquals(children, family.getChildren());
    }


    @Test
    void testDeleteChildIndex() {
        Human[] children = {new Human("Олексій", "efee", 343), new Human("Олена", "32434", 3443)};
        family.setChildren(children);
        int childRemoveIndex = 0;

        family.deleteChild(0);
        Human[] updatedChildren = family.getChildren();
        assertEquals(1, updatedChildren.length);
        assertEquals(children[1], updatedChildren[0]);
    }

    @Test
    void testDeleteChildInvalidIndex() {
        Human[] children = {new Human("Олексій", "efee", 343), new Human("Олена", "32434", 3443)};
        Human nonExistingChild = new Human("О", "efegge", 34333);

        int invalidIndex = 2;
        family.setChildren(children);
        boolean result = family.deleteChild(2);

        assertFalse(result);
        assertArrayEquals(children, family.getChildren());
    }

    @Test
    void testToString() {
        mother = new Human("Олена", "32434", 3443);
        father = new Human("Олексій", "efee", 343);
        family = new Family(mother, father);

        String expected = "Human {name = 'Олексій', surname = 'efee', year = 343, iq = 0, null}\n" +
                "Human {name = 'Олена', surname = '32434', year = 3443, iq = 0, null}\n";
        assertEquals(expected, family.toString());
    }

    @Test
    void testToStringNull() {
        family = new Family(null, null);
        String expected = "Human {name = 'null', surname = 'null', year = 0, iq = 0, null}\n" +
                "Human {name = 'null', surname = 'null', year = 0, iq = 0, null}\n";
        assertEquals(expected, family.toString());
    }
}

