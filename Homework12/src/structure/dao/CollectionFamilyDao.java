package structure.dao;

import structure.model.Family;
import structure.model.Human;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao{
    private List<Family> families;

    public CollectionFamilyDao() {
        this.families = new ArrayList<>();
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
            return families.get(index);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        if (families.contains(family)) {
            int index = families.indexOf(family);
            families.set(index, family);
        } else {
            families.add(family);
        }
    }

    public int getFamilySize(Family family) {
        return family.getChildren().size() + 2;
    }

    @Override
    public void setChild(Family family, Human child) {
        family.getChildren().add(child);
    }

    @Override
    public void loadData(List<Family> families){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("families.txt"));
            oos.writeObject(families);
            oos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void downloadData() {
        try {
            this.families = (List<Family>) new ObjectInputStream(new FileInputStream("families.txt")).readObject();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }catch (FileNotFoundException e){
            System.out.println("Файл не знайдено");
        }catch (EOFException e){
            System.out.println("Даних немає");
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}


