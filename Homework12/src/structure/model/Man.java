package structure.model;

import java.util.Map;
import java.util.Set;

public final class Man extends Human{
    public void drinkBeer(){
        System.out.println("Йду пити пиво з друзями");
    }

    @Override
    public void greetPet() {
        Set<Pet> pets = getFamily().getPets();
        for (Pet pet : pets) {
            System.out.println("Здоров, " + pet.getNickname());
        }
    }


    public Man() {
    }

    public Man(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, long birthDate, int iq, Map<DayOfWeek,String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Man(String name, String surname, String birthDateStr, int iq) {
        super(name, surname, birthDateStr, iq);
    }
}
