package structure.model;

import java.io.Serializable;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Human implements Serializable {
    Random random = new Random();
    private Map<DayOfWeek, String> schedule = new HashMap<>();
    private DayOfWeek dayOfWeek;
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;


    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

        public Set<Pet> getPets() {
        return getFamily().getPets();
    }
    public String getName() {
        return name;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    @Override
    public String toString() {
        LocalDate birthDateLocal = LocalDate.ofInstant(Instant.ofEpochMilli(birthDate), ZoneId.systemDefault());
        String formattedBirthDate = birthDateLocal.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return String.format("Human {name = '%s', surname = '%s', birthDate = %s, iq = %d, %s}", this.name, this.surname, formattedBirthDate,
                this.iq, this.schedule);
    }

    public void greetPet() {
        if (family != null) {
            Set<Pet> pets = family.getPets();
            for (Pet pet : pets) {
                System.out.println("Привіт, " + pet.getNickname());
            }
        }
    }

    public void describePet() {
        if (family != null) {
            Set<Pet> pets = family.getPets();
            for (Pet pet : pets) {
                if (pet.getTrickLevel() <= 50) {
                    System.out.println("В мене є " + pet.getSpecies() + ", йому " + pet.getAge() + " років, він майже не хитрий");
                } else {
                    System.out.println("В мене є " + pet.getSpecies() + ", йому " + pet.getAge() + " років, він дуже хитрий");
                }
            }
        }
    }

    public boolean feedPet(boolean timeToEat) {
        if (family != null) {
            Set<Pet> pets = family.getPets();
            for (Pet pet : pets) {
                int extraEating = random.nextInt(101);
                if (timeToEat) {
                    System.out.println("Годую вихованця");
                    return true;
                } else if (pet.getTrickLevel() >= extraEating) {
                    System.out.println("Хм... напевно поголдую я " + pet.getNickname());
                    return true;
                } else {
                    System.out.println("Думаю, " + pet.getNickname() + " не голодний.");
                    return false;
                }
            }
        }
        return false;
    }

    public String describeAge() {
        LocalDate currentDate = LocalDate.now();
        Instant instant = Instant.ofEpochMilli(birthDate);
        LocalDate birthDateTime = LocalDate.ofInstant(instant, ZoneId.systemDefault());

        long years = birthDateTime.until(currentDate, ChronoUnit.YEARS);
        birthDateTime = birthDateTime.plusYears(years);

        long months = birthDateTime.until(currentDate, ChronoUnit.MONTHS);
        birthDateTime = birthDateTime.plusMonths(months);

        long days = birthDateTime.until(currentDate, ChronoUnit.DAYS);

        return String.format("%d years, %d months, and %d days", years, months, days);
    }

    public String prettyFormat(){
        LocalDate birthDateLocal = LocalDate.ofInstant(Instant.ofEpochMilli(birthDate), ZoneId.systemDefault());
        String formattedBirthDate = birthDateLocal.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return String.format("{name = '%s', surname = '%s', birthDate = %s, iq = %d, %s}", this.name, this.surname, formattedBirthDate,
                this.iq, this.schedule);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        if (human.hashCode() != o.hashCode()) {
            return false;
        }

        if (birthDate != human.birthDate || iq != human.iq) {
            return false;
        }
        if (name == null && human.name == null) {
            return true;
        }
        if (name == null || human.name == null) {
            return false;
        }
        if (!name.equals(human.name)) {
            return false;
        }
        return surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq);
    }

    @Override
    protected void finalize() {
        System.out.println("model.Human is deleting");
    }

    public Human() {
    }

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human( String name, String surname, long birthDate, int iq, Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }

    public Human(String name, String surname, String birthDateStr, int iq) {
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate birthDate = LocalDate.parse(birthDateStr, formatter);
        LocalDateTime startOfDay = birthDate.atStartOfDay();
        this.birthDate = startOfDay.toInstant(ZoneOffset.UTC).toEpochMilli();
        this.iq = iq;
    }
}
