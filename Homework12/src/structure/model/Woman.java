package structure.model;

import java.util.Map;
import java.util.Set;

public final class Woman extends Human{


    public void goToSalon() {
        System.out.println("Йду в салон краси");
    }

    @Override
    public void greetPet() {
        Set<Pet> pets = getFamily().getPets();
        for (Pet pet : pets) {
            System.out.println("Мій улюблений , " + pet.getNickname());
        }
    }

    public Woman() {
    }

    public Woman(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, long birthDate, int iq, Map<DayOfWeek,String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Woman(String name, String surname, String birthDateStr, int iq) {
        super(name, surname, birthDateStr, iq);
    }
}
